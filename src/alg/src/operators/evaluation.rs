use super::mapping::RouteNode;
use crate::models::{queueing_model::QueueingModel, service::Service};
use std::cmp::Ordering;

#[derive(Copy, Clone, Debug, PartialEq)]
pub struct VnfppFitness {
    pub num_unplaced: usize,
    pub sum_constraint_violation: f64,
    pub energy: f64,
}

impl VnfppFitness {
    pub fn new() -> VnfppFitness {
        VnfppFitness {
            num_unplaced: std::usize::MAX,
            sum_constraint_violation: std::f64::MAX,
            energy: std::f64::MAX,
        }
    }
}

impl PartialOrd for VnfppFitness {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        if self.num_unplaced < other.num_unplaced {
            return Some(Ordering::Greater);
        } else if self.num_unplaced > other.num_unplaced {
            return Some(Ordering::Less);
        }

        if self.sum_constraint_violation > 0.0 || other.sum_constraint_violation > 0.0 {
            if self.sum_constraint_violation < other.sum_constraint_violation {
                return Some(Ordering::Greater);
            } else if self.sum_constraint_violation > other.sum_constraint_violation {
                return Some(Ordering::Less);
            }
        }

        if self.energy < other.energy {
            return Some(Ordering::Greater);
        } else if self.energy > other.energy {
            return Some(Ordering::Less);
        }

        Some(Ordering::Equal)
    }
}

pub trait Evaluation {
    fn evaluate_ind(&mut self, routes: &Vec<(usize, Vec<RouteNode>)>) -> VnfppFitness;
}

// --- Queueing Model
#[derive(Clone)]
pub struct QueueingEval<'a> {
    pub queueing_model: QueueingModel<'a>,
    services: &'a Vec<Service>,
    latency_constraints: &'a Vec<f64>,
    pl_constraints: &'a Vec<f64>,
    pub use_hf_cnstr: bool,
}

impl<'a> QueueingEval<'a> {
    pub fn new(
        queueing_model: QueueingModel<'a>,
        services: &'a Vec<Service>,
        latency_constraints: &'a Vec<f64>,
        pl_constraints: &'a Vec<f64>,
    ) -> QueueingEval<'a> {
        QueueingEval {
            queueing_model,
            services,
            latency_constraints,
            pl_constraints,
            use_hf_cnstr: true,
        }
    }
}

impl Evaluation for QueueingEval<'_> {
    fn evaluate_ind(&mut self, routes: &Vec<(usize, Vec<RouteNode>)>) -> VnfppFitness {
        let num_unplaced = num_unplaced(&routes, self.services);

        let (latencies, packet_losses, energy) =
            self.queueing_model.evaluate(&self.services, &routes);

        let mut sum_constraint_violation = 0.0;

        for i in 0..self.services.len() {
            let latency_violation =
                (latencies[i] - self.latency_constraints[i]) / self.latency_constraints[i];
            let pl_violation = (packet_losses[i] - self.pl_constraints[i]) / self.pl_constraints[i];

            sum_constraint_violation = sum_constraint_violation + latency_violation + pl_violation;
        }

        VnfppFitness {
            num_unplaced,
            sum_constraint_violation,
            energy,
        }
    }
}

// --- Helpers
pub fn num_unplaced(routes: &Vec<(usize, Vec<RouteNode>)>, services: &Vec<Service>) -> usize {
    let mut counts = vec![0; services.len()];
    routes.iter().for_each(|&(s_id, _)| {
        counts[s_id] += 1;
    });

    counts.iter().filter(|&&count| count == 0).count()
}
