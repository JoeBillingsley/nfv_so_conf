use super::evaluation::VnfppFitness;

use std::{
    fmt::Debug,
    ops::{Index, IndexMut},
};

#[derive(Clone, Debug)]
pub struct Solution<X> {
    pub genotype: Vec<X>,
    pub fitness: VnfppFitness,
}

impl<X> Solution<X> {
    pub fn new(point: Vec<X>) -> Solution<X> {
        Solution {
            genotype: point,
            fitness: VnfppFitness::new(),
        }
    }

    pub fn len(&self) -> usize {
        self.genotype.len()
    }
}

impl<X> Index<usize> for Solution<X> {
    type Output = X;

    fn index(&self, i: usize) -> &Self::Output {
        &self.genotype[i]
    }
}

impl<X> IndexMut<usize> for Solution<X> {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        &mut self.genotype[index]
    }
}
