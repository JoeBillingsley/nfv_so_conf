use crate::operators::{
    crossover::Crossover, evaluation::Evaluation, initialisation::InitPop, mapping::Mapping,
    mutation::Mutation, selection::TournamentSelection, solution::Solution,
};
use std::fmt::Debug;

pub fn run<
    X,
    Init: InitPop<X>,
    Map: Mapping<X>,
    Mutate: Mutation<X>,
    Eval: Evaluation + Sync,
    Cross: Crossover<X>,
>(
    init_pop: &Init,
    mapping: &Map,
    evaluation: &mut Eval,
    mutation: &Mutate,
    crossover: &Cross,
    pop_size: usize,
    max_evaluations: usize,
    mut iteration_observer: impl FnMut(usize, &Vec<Solution<X>>),
) where
    X: Clone + Debug,
{
    // Initialise population
    let mut parent_pop = init_pop.apply(pop_size);
    for ind in &mut parent_pop {
        let routes = mapping.apply(ind);
        ind.fitness = evaluation.evaluate_ind(&routes);
    }

    let mut evaluations = parent_pop.len();

    let mut child_pop = Vec::with_capacity(pop_size);
    let mut combined_pop = Vec::with_capacity(pop_size * 2);

    while evaluations < max_evaluations {
        iteration_observer(evaluations, &parent_pop);

        let ts = TournamentSelection::new(parent_pop.len(), |x, y| {
            parent_pop[x].fitness > parent_pop[y].fitness
        });

        while child_pop.len() < pop_size {
            // Tournament selection to decide parents
            let parent_one = ts.tournament(2);
            let parent_two = ts.tournament(2);

            // Crossover
            let new_children = crossover.apply(&parent_pop[parent_one], &parent_pop[parent_two]);

            // Mutation
            for child in new_children {
                let mut child_mut = mutation.apply(&child);

                let routes = mapping.apply(&child_mut);
                child_mut.fitness = evaluation.evaluate_ind(&routes);

                child_pop.push(child_mut);
            }
        }

        evaluations += child_pop.len();

        combined_pop.append(&mut parent_pop);
        combined_pop.append(&mut child_pop);

        // Top selection
        combined_pop.sort_by(|x, y| x.fitness.partial_cmp(&y.fitness).unwrap());
        combined_pop.reverse();

        for ind in combined_pop.drain(0..pop_size) {
            parent_pop.push(ind);
        }

        combined_pop.clear();
        child_pop.clear();
    }

    // Final population
    iteration_observer(evaluations, &parent_pop);
}
