use crate::{
    models::{queueing_model::QueueingModel, service::Service},
    operators::{
        mapping::{IntStringToRouteMapping, Mapping},
        placement_strategies::NodeSelection,
        solution::Solution,
    },
};

pub fn run<N>(
    services: &Vec<Service>,
    mapping: IntStringToRouteMapping<N>,
    mut model: QueueingModel,
    latencies_cnstr: &Vec<f64>,
    pl_cnstr: &Vec<f64>,
    max_evaluations: usize,
    mut iteration_observer: impl FnMut(usize, &Vec<Solution<usize>>),
) where
    N: NodeSelection + Clone,
{
    let mut evaluations = 0;

    let mut max_infeasible = vec![Some(0); services.len()];
    let mut min_feasible = vec![None; services.len()];

    let mut solution = Solution::new(vec![1; services.len()]);

    loop {
        let routes = mapping.apply(&solution);

        // Count the number of placed instances of each service
        let mut counts = vec![0; services.len()];
        routes.iter().for_each(|&(s_id, _)| {
            counts[s_id] += 1;
        });

        let (latencies, packet_losses, _) = model.evaluate(&services, &routes);

        iteration_observer(evaluations, &vec![solution.clone()]);

        let mut sum_constraint_violation = 0.0;

        for i in 0..services.len() {
            let latency_violation = (latencies[i] - latencies_cnstr[i]) / latencies_cnstr[i];
            let pl_violation = (packet_losses[i] - pl_cnstr[i]) / pl_cnstr[i];

            sum_constraint_violation = sum_constraint_violation + latency_violation + pl_violation;
        }

        for i in 0..services.len() {
            if counts[i] == 0 {
                // Indicates an issue with the available space or the placement
                // - not necessarily the number of instances
                continue;
            }

            let is_feasible = latencies[i] < latencies_cnstr[i] || packet_losses[i] < pl_cnstr[i];

            if is_feasible {
                min_feasible[i] = Some(solution[i]);
            } else {
                max_infeasible[i] = Some(solution[i]);
            }

            // if solution[i] > 32 {
            //     println!(
            //         "{}: {} {}. {} {}. {} {}. {}.",
            //         i,
            //         latencies[i],
            //         latencies_cnstr[i],
            //         packet_losses[i],
            //         pl_cnstr[i],
            //         services[i].prod_rate,
            //         services[i].vnfs.len(),
            //         counts[i]
            //     );

            //     for vnf in &services[i].vnfs {
            //         println!("{}", vnf.service_rate);
            //     }
            // }

            if min_feasible[i].is_none() {
                solution[i] = max_infeasible[i].unwrap() * 2;
            } else {
                let max_inf = max_infeasible[i].unwrap() as f64;
                let min_fea = min_feasible[i].unwrap() as f64;

                solution[i] = ((max_inf + min_fea) / 2.0).ceil() as usize;
            }
        }

        evaluations = evaluations + 1;

        if evaluations > max_evaluations {
            break;
        }
    }
}
