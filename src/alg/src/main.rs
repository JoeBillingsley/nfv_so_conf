mod algorithms;
mod models;
mod operators;
mod utilities;

use algorithms::{bisection_search, genetic_algorithm};
use models::{
    datacentre::{Datacentre, Topology},
    queueing_model::QueueingModel,
    routing::RoutingTable,
    service::{Service, VNF},
};
use operators::{
    crossover::UniformCrossover,
    distance_matrix::{self, DistanceMatrix},
    evaluation::QueueingEval,
    initialisation::{SAIntStringInitialisation, ServiceAwareInitialisation},
    mapping::{IntStringToRouteMapping, ServiceToRouteMapping},
    mutation::{AddRemoveMutation, IncrDecrMutation},
    placement_strategies::FirstFit,
    solution::Solution,
};
use rand::thread_rng;
use rand_distr::{Distribution, Normal};
use std::{
    fs::{self, File, OpenOptions},
    io::{BufReader, BufWriter, Write},
    path::PathBuf,
};

fn main() {
    let mut settings = config::Config::default();
    settings
        .merge(config::File::with_name("Config"))
        .unwrap()
        .merge(config::Environment::with_prefix("APP"))
        .unwrap();

    // Get output folder
    let results_folder: String = settings.get("results_folder").unwrap();
    let results_folder = PathBuf::new().join(&results_folder);

    run_basic_tests(&results_folder, 1200);
}

fn run_basic_tests(out_folder: &PathBuf, max_evaluations: usize) {
    // --- Test Problem ---
    let topologies = [Topology::FatTree, Topology::LeafSpine, Topology::DCell];
    let scales = [500, 1000, 2000, 4000, 8000];

    let sw_sr = 20.0;
    let sw_ql = 20;
    let accuracy = 5.0;
    let converged_iterations = 10;
    let active_cost = 30.0;
    let idle_cost = 10.0;

    let utilisation = 0.6;

    for topology in topologies.iter() {
        for scale in scales.iter() {
            let (dc, rt) = load_topology(&topology, *scale);

            // Mapping + Fitness function
            let num_nearest = 50;
            let dm = distance_matrix::build_cache(&dc, num_nearest);

            let capacities = vec![100; dc.num_servers];

            let qm = QueueingModel::new(
                &dc,
                sw_sr,
                sw_ql,
                accuracy,
                converged_iterations,
                active_cost,
                idle_cost,
            );

            for pi in 0..30 {
                let (services, latency_cnstr, pl_cnstr) =
                    create_problem_instance(dc.num_servers, utilisation);

                // Bisection
                run_bisection_search_test(
                    pi,
                    &qm,
                    &dc,
                    &rt,
                    &services,
                    &latency_cnstr,
                    &pl_cnstr,
                    max_evaluations,
                    out_folder,
                );

                // Integer - String genetic algorithm
                run_instances_pl_ga_test(
                    pi,
                    &qm,
                    &dc,
                    &capacities,
                    &dm,
                    &rt,
                    &services,
                    &latency_cnstr,
                    &pl_cnstr,
                    max_evaluations,
                    out_folder,
                );

                run_service_pl_ga(
                    pi,
                    &qm,
                    &dc,
                    &capacities,
                    &dm,
                    &rt,
                    &services,
                    &latency_cnstr,
                    &pl_cnstr,
                    max_evaluations,
                    out_folder,
                );
            }
        }
    }
}

fn run_bisection_search_test(
    id: usize,
    qm: &QueueingModel,
    dc: &Datacentre,
    rt: &Vec<RoutingTable>,
    services: &Vec<Service>,
    latency_cnstr: &Vec<f64>,
    pl_cnstr: &Vec<f64>,
    max_evaluations: usize,
    out_folder: &PathBuf,
) {
    // Mapping + Fitness function
    let num_nearest = 50;
    let dm = distance_matrix::build_cache(&dc, num_nearest);

    let capacities = vec![100; dc.num_servers];
    let mapping = IntStringToRouteMapping::new(FirstFit::new(), &services, &capacities, &dm, &rt);

    bisection_search::run(
        &services,
        mapping,
        qm.clone(),
        latency_cnstr,
        pl_cnstr,
        max_evaluations,
        |eval, pop| {
            if eval == 0 || eval % 120 == 0 {
                print_best(pop, out_folder, services.len(), id).unwrap();
            }
        },
    );
}

fn run_instances_pl_ga_test(
    id: usize,
    qm: &QueueingModel,
    dc: &Datacentre,
    capacities: &Vec<usize>,
    dm: &DistanceMatrix,
    rt: &Vec<RoutingTable>,
    services: &Vec<Service>,
    latency_cnstr: &Vec<f64>,
    pl_cnstr: &Vec<f64>,
    max_evaluations: usize,
    out_folder: &PathBuf,
) {
    // --- Genetic Operators ---
    let mut eval = QueueingEval::new(qm.clone(), services, latency_cnstr, pl_cnstr);

    // Initialisation
    let init_pop = SAIntStringInitialisation::new(&services, dc.num_servers);

    // Mapping
    let mapping = IntStringToRouteMapping::new(FirstFit::new(), &services, &capacities, &dm, &rt);

    // Mutation
    let pm = 0.05;
    let mr = 0.4;

    let mutation = IncrDecrMutation::new(pm, mr);

    // Crossover
    let pc = 0.4;
    let crossover = UniformCrossover::new(pc);

    let pop_size = 120;

    genetic_algorithm::run(
        &init_pop,
        &mapping,
        &mut eval,
        &mutation,
        &crossover,
        pop_size,
        max_evaluations,
        |evaluations, pop| {
            if evaluations == 0 || evaluations % 120 == 0 {
                print_best(pop, out_folder, services.len(), id).unwrap();
            }
        },
    );
}

fn run_service_pl_ga(
    id: usize,
    qm: &QueueingModel,
    dc: &Datacentre,
    capacities: &Vec<usize>,
    dm: &DistanceMatrix,
    rt: &Vec<RoutingTable>,
    services: &Vec<Service>,
    latency_cnstr: &Vec<f64>,
    pl_cnstr: &Vec<f64>,
    max_evaluations: usize,
    out_folder: &PathBuf,
) {
    // --- Genetic Operators ---
    let mut eval = QueueingEval::new(qm.clone(), services, &latency_cnstr, &pl_cnstr);

    // Initialisation
    let init_pop = ServiceAwareInitialisation::new(&services, dc.num_servers, capacities);

    // Mapping
    let mapping = ServiceToRouteMapping::new(FirstFit::new(), &capacities, &dm, &rt);

    // Mutation
    let pm = 0.05;
    let mr = 0.4;
    let items = services.iter().map(|s| s).collect();
    let mutation = AddRemoveMutation::new(items, pm, mr);

    // Crossover
    let pc = 0.4;
    let crossover = UniformCrossover::new(pc);

    let pop_size = 120;

    genetic_algorithm::run(
        &init_pop,
        &mapping,
        &mut eval,
        &mutation,
        &crossover,
        pop_size,
        max_evaluations,
        |evaluations, pop| {
            if evaluations == 0 || evaluations % 120 == 0 {
                print_best(pop, out_folder, services.len(), id).unwrap();
            }
        },
    );
}

// ----- Helper functions ------
fn create_problem_instance(
    num_servers: usize,
    utilisation: f64,
) -> (Vec<Service>, Vec<f64>, Vec<f64>) {
    // Problem parameter settings
    let mean_service_len = 5.0;
    let variance_service_len = 1.0;

    let mean_prod_rate = 10.0;
    let variance_prod_rate = 3.0;

    let mean_service_rate = 10.0;
    let variance_service_rate = 3.0;

    let queue_length = 50;

    let mean_size = 40.0;
    let variance_size = 10.0;

    // Distributions
    let service_len_distr = Normal::new(mean_service_len, variance_service_len).unwrap();
    let prod_rate_distr = Normal::new(mean_prod_rate, variance_prod_rate).unwrap();
    let service_rate_distr = Normal::new(mean_service_rate, variance_service_rate).unwrap();
    let size_distr = Normal::new(mean_size, variance_size).unwrap();

    let num_services =
        (utilisation * (1.0 / mean_service_len) * num_servers as f64).max(1.0) as usize;

    let mut rng = thread_rng();
    let mut services;

    loop {
        services = Vec::new();

        let mut vnf_id = 0;
        for service_id in 0..num_services {
            let prod_rate: f64 = prod_rate_distr.sample(&mut rng);
            let prod_rate = prod_rate.max(2.0);

            let mut service = Service {
                id: service_id,
                prod_rate,
                vnfs: Vec::new(),
            };

            let num_vnfs = service_len_distr.sample(&mut rng).max(2.0).min(12.0);
            let num_vnfs = num_vnfs as usize;

            for _ in 0..num_vnfs {
                let service_rate: f64 = service_rate_distr.sample(&mut rng);
                let service_rate = service_rate.max(2.0);

                let size: f64 = size_distr.sample(&mut rng);
                let size = size.min(100.0).max(1.0) as usize;

                service.vnfs.push(VNF {
                    service_rate,
                    queue_length,
                    size,
                });

                vnf_id = vnf_id + 1;
            }

            services.push(service);
        }

        let mut used_capacity = 0;
        for service in &services {
            for vnf in &service.vnfs {
                used_capacity = used_capacity + vnf.size;
            }
        }

        // Filter out some unsolveable problems
        let total_capacity = 100 * num_servers;
        if used_capacity <= total_capacity {
            break;
        }
    }

    let mut latency_cnstr = Vec::with_capacity(services.len());
    let mut pl_cnstr = Vec::with_capacity(services.len());

    let latency_distr = Normal::new(3.0, 1.0).unwrap();
    let pl_distr = Normal::new(0.3, 0.1).unwrap();

    for _ in 0..services.len() {
        let lc: f64 = latency_distr.sample(&mut rng);
        let lc = lc.max(0.4);
        latency_cnstr.push(lc);

        let plc: f64 = pl_distr.sample(&mut rng);
        let plc = plc.max(0.01);
        pl_cnstr.push(plc);
    }

    (services, latency_cnstr, pl_cnstr)
}

fn load_topology(topology: &Topology, size: usize) -> (Datacentre, Vec<RoutingTable>) {
    let file = File::open(format!("topology/{}_{}.dat", topology, size)).unwrap();
    let reader = BufReader::new(file);
    let dc: Datacentre = bincode::deserialize_from(reader).unwrap();

    let file = File::open(format!("topology/{}_routing_{}.dat", topology, size)).unwrap();
    let reader = BufReader::new(file);
    let rt: Vec<RoutingTable> = bincode::deserialize_from(reader).unwrap();

    (dc, rt)
}

fn print_best<X>(
    pop: &Vec<Solution<X>>,
    folder: &PathBuf,
    num_services: usize,
    id: usize,
) -> std::io::Result<()> {
    let file_name = format!("{}_{}.fit", num_services, id);
    let mut file = get_file(&folder, &file_name).unwrap();

    let mut best_ind = &pop[0];

    for ind in pop {
        if ind.fitness > best_ind.fitness {
            best_ind = ind;
        }
    }

    let best_fitness = best_ind.fitness;

    write!(
        file,
        "{}, {}, {}",
        best_fitness.num_unplaced, best_fitness.sum_constraint_violation, best_fitness.energy
    )?;

    Ok(())
}

fn get_file(folder: &PathBuf, file: &str) -> std::io::Result<BufWriter<File>> {
    fs::create_dir_all(folder).unwrap();
    let path = folder.join(file);

    let file = OpenOptions::new()
        .read(true)
        .write(true)
        .create(true)
        .open(path)
        .unwrap();

    Ok(BufWriter::new(file))
}
